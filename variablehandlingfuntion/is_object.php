<?php
//Declared  a simple funtion to return an 
// array from our object
function get_students($obj)
{
	if(!is_object($obj)) {
		return false;
	}
	
	return $obj->students;
}

// Declared a new class instance  and fill up 
// Some Values
 $obj = new stdClass();
 $obj ->students = array('Kalle','Ross','Felipe');
 
 var_dump(get_students(null));
 var_dump(get_students($obj));
 ?>